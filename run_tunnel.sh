#!/bin/bash
# script to setup and run lambdatest tunnel

curl -O https://downloads.lambdatest.com/tunnel/v3/linux/64bit/LT_Linux.zip
unzip LT_Linux.zip
rm LT_Linux.zip
chmod +x LT

#find random free port
read LOWERPORT UPPERPORT </proc/sys/net/ipv4/ip_local_port_range
PORT=$LOWERPORT
while [ $PORT -lt $UPPERPORT ]; do
    ss -lpn | grep -q ":$PORT " || break
    let PORT=PORT+1
done

echo "$PORT" >/tmp/port

#start tunnel in background use command line flags uses default variable BITBUCKET_STEP_UUID for unqiue tunnel name
./LT --user $LAMBDATEST_USERNAME --key $LAMBDATEST_KEY --tunnelName $BITBUCKET_STEP_UUID --infoAPIPort $PORT --controller bitbucket &

# wait for tunnel to start, note curl version should be >= 7.52.0 for flag -retry-connrefused
curl  --silent --retry-connrefused --connect-timeout 5 --max-time 5 --retry 30 --retry-delay 2 --retry-max-time 60 http://127.0.0.1:$PORT/api/v1.0/info 2>&1 > /dev/null
